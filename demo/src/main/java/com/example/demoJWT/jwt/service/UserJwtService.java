package com.example.demoJWT.jwt.service;

import com.example.demoJWT.jwt.model.CustomUserDetails;
import com.example.demoJWT.model.jpa.AccountEntity;
import com.example.demoJWT.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserJwtService implements UserDetailsService {

    @Autowired
    private AccountService accountService; // tao ra account

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AccountEntity accountEntity = accountService.getAccountByUsername(username);

        if (accountEntity == null) {
            throw new UsernameNotFoundException(username);
        } else return new CustomUserDetails(accountEntity);
    }

    public CustomUserDetails loadUserByUserId(Integer userId) {
        AccountEntity accountEntity = accountService.getAccountByUserId(userId);
        if (accountEntity == null) {
            throw new UsernameNotFoundException("userId" + userId);
        } else return new CustomUserDetails(accountEntity);
    }
}
