package com.example.demoJWT.jwt;

import com.example.demoJWT.jwt.model.CustomUserDetails;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class JwtTokenProvider {

    // Đoạn JWT_SECRET này là bí mật, chỉ có phía server biết
    private final String JWT_SECRET = "zegatea";

    //Thời gian có hiệu lực của chuỗi jwt
    private final long JWT_EXPIRATION = 15 * 24 * 60 * 60 * 1000L;

    // Tạo ra jwt từ thông tin user
    public String generateToken(CustomUserDetails userDetails) {
        Date now = new Date();
        Date expireDate = new Date(now.getTime() + JWT_EXPIRATION);
        // Tạo chuỗi json web token từ id của user.
        System.out.println("create token for username [{" + userDetails.getUsername().toString() + "}] valid from [{" + now.toString()+"}] to [{" +expireDate.toString() +"}]");
        return Jwts.builder()
                .setSubject(Long.toString(userDetails.getAccount().getUserByUserId().getUserId()))
                .setAudience(userDetails.getAccount().getUserName())
                .setIssuedAt(now)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }

    // Lấy thông tin user từ jwt
    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();
        Long subject = Long.parseLong(claims.getSubject());

        return subject;
    }

    public Claims getTokenRaw(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();
        return claims;
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            System.out.println("Invalid JWT token: {}" +  ex.getMessage());
        } catch (ExpiredJwtException ex) {
            System.out.println("Expired JWT token: {}" + ex.getMessage());
        } catch (UnsupportedJwtException ex) {
            System.out.println("Unsupported JWT token: {}" + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            System.out.println("JWT claims string is empty: {}" + ex.getMessage());
        }
        return false;
    }
}
