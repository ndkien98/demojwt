package com.example.demoJWT.jwt;

import com.example.demoJWT.util.EncryptUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

public class CustomPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {   // tao ra password
        return EncryptUtils.crc32PasswordEncoder(charSequence.toString());
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) { // so sanh pass duoc tao ra va chuoi truyen vao
        return s.equals(EncryptUtils.crc32PasswordEncoder(charSequence.toString()));
    }
}
