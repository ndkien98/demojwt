package com.example.demoJWT.controller;

import com.example.demoJWT.model.request.ImgRequest;
import com.example.demoJWT.model.response.BaseResponse;
import com.example.demoJWT.service.ImageService;
import com.example.demoJWT.util.Global;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/image")
public class ImageController {


    @Autowired
    ImageService imageService;

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<String> updateImg(@RequestBody MultipartFile file) {
        try {

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setData(this.imageService.uploadImg(file));
            baseResponse.setErrorCode(200);
            baseResponse.setMessage("upload img success");
            return ResponseEntity.ok(Global.gsonResponse.toJson(baseResponse));
        } catch (IOException e) {
            BaseResponse baseResponse = new BaseResponse();
            e.printStackTrace();
            return new ResponseEntity(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }
    }
}
