package com.example.demoJWT.controller;

import com.example.demoJWT.config.StatusCode;
import com.example.demoJWT.jwt.JwtTokenProvider;
import com.example.demoJWT.jwt.model.CustomUserDetails;
import com.example.demoJWT.model.jpa.UserEntity;
import com.example.demoJWT.model.request.LoginRequest;
import com.example.demoJWT.model.response.BaseResponse;
import com.example.demoJWT.model.response.LoginResponse;
import com.example.demoJWT.service.AccountService;
import com.example.demoJWT.service.UserService;
import com.example.demoJWT.util.Global;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/account", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AccountController {


    @Autowired
    AccountService accountService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<String> login(@RequestBody(required = true) LoginRequest loginRequest) {

        BaseResponse baseResponse = new BaseResponse();

        if (loginRequest == null || loginRequest.getUsername().isEmpty() || loginRequest.getPassword().isEmpty()) {
            baseResponse.setMessage(" Username and password must not null or empty ");
            baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
            return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }

        int userIdCheckLogin = accountService.checkLogin(loginRequest.getUsername(), loginRequest.getPassword());
        if (userIdCheckLogin == -1) {
            baseResponse.setMessage("Username or password in incorrect");
            baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
            return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }

        UserEntity userEntity = userService.getUserById(userIdCheckLogin);
        if (userEntity == null) {
            baseResponse.setErrorCode(StatusCode.ERROR);
            baseResponse.setMessage("Can not get infor user");
            return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtTokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
        LoginResponse loginResponse = new LoginResponse(jwt, userEntity);
        baseResponse.setMessage(" thanh cong");
        baseResponse.setData(loginResponse);
        baseResponse.setErrorCode(StatusCode.SUCCESS);
        return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.OK);
    }

}
