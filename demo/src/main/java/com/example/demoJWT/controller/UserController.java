package com.example.demoJWT.controller;

import com.example.demoJWT.config.StatusCode;
import com.example.demoJWT.model.jpa.UserEntity;
import com.example.demoJWT.model.request.UserRequest;
import com.example.demoJWT.model.response.BaseResponse;
import com.example.demoJWT.service.UserService;
import com.example.demoJWT.util.Global;
import com.example.demoJWT.util.LogicUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestBody UserRequest userRequest) {

        BaseResponse baseResponse = new BaseResponse();

        LogicUtils logicUtils = new LogicUtils();

        if (userRequest == null || logicUtils.checkNullOrEmpty(userRequest.getUserCode(), userRequest.getFname())) {
            baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
            baseResponse.setMessage("username of full name must not null");
            return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }

        UserEntity userEntity = userService.getUserByUserCode(userRequest.getUserCode());
        if (userEntity != null) {
            baseResponse.setMessage("user code existed");
            baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
            return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }

        int userId = 0;
        try {
            userId = userService.createUser(userRequest);
        } catch (IOException e) {
            e.printStackTrace();
            baseResponse.setMessage("bad request");
            baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
            return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }
        if (userId == -1) {
            baseResponse.setErrorCode(StatusCode.INVALID_INFORMATION);
            baseResponse.setMessage("Creates user error, please check user's infor");
            return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.BAD_REQUEST);
        }

        baseResponse.setMessage("Create user success");
        baseResponse.setErrorCode(StatusCode.SUCCESS);
        baseResponse.setData(userService.getUserById(userId));

        return new ResponseEntity<>(Global.gsonResponse.toJson(baseResponse), HttpStatus.OK);
    }


}
