package com.example.demoJWT.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Global {
    public static Gson gsonResponse = new GsonBuilder().setPrettyPrinting()
            .excludeFieldsWithoutExposeAnnotation()
            .create();
}
