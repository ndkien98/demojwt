package com.example.demoJWT.dao.repository;

import com.example.demoJWT.model.jpa.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccountReposiroty extends JpaRepository<AccountEntity,Integer> {


    @Query("SELECT AC FROM AccountEntity AC WHERE AC.userId =:userId")
    AccountEntity getAccountByUserId(@Param("userId") Integer userId);

    @Query("SELECT AC FROM AccountEntity AC WHERE AC.userName =:username")
    AccountEntity getAccountByUserName(@Param("username") String username);

    @Query("SELECT AC FROM AccountEntity AC WHERE AC.userName =:username AND AC.passwordHash =:password")
    AccountEntity getUserLogin(@Param("username") String username, @Param("password") String password);
}
