package com.example.demoJWT.dao.repository;

import com.example.demoJWT.model.jpa.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Integer> {

    UserEntity findByUserCode(@Param("userCode") String userCode);

}
