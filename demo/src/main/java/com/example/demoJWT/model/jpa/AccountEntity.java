package com.example.demoJWT.model.jpa;

import com.google.gson.annotations.Expose;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "account", schema = "demojwt")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int acountId;

    @Basic
    @Column(name = "userCode")
    @Expose
    private String userCode;

    @Column(name = "userName")
    private String userName;

    @Column(name = "passwordHash")
    private String passwordHash;

    @Column(name = "userId", insertable = false, updatable = false)
    @Expose
    private int userId;

    @ManyToOne // nhieu account co the co cung 1 role
    @JoinColumn(name = "roleId", referencedColumnName = "roleId", nullable = false)
    @EqualsAndHashCode.Exclude
    @Expose
    private RoleEntity roleByRoleId;
    @OneToOne // 1 account chi cho 1 user duy nhat
    @JoinColumn(name = "userId", referencedColumnName = "userId", nullable = false)
    @EqualsAndHashCode.Exclude
    @Expose
    private UserEntity userByUserId;

    public AccountEntity(String userName, String passwordHash, RoleEntity roleEntity, UserEntity userByUserId) {
        this.userName = userName;
        this.passwordHash = passwordHash;
        this.roleByRoleId = roleEntity;
        this.userByUserId = userByUserId;
    }

}
