package com.example.demoJWT.model.jpa;

import com.google.gson.annotations.Expose;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "role", schema = "demojwt")
@Data
@NoArgsConstructor
public class RoleEntity {
    @Id
    @Column(name = "roleId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private int roleId;
    @Basic
    @Column(name = "roleName")
    @Expose
    private String roleName;
    @Basic
    @Column(name = "roleValue")
    @Expose
    private Integer roleValue;

    public RoleEntity(String roleName, Integer roleValue) {
        this.roleName = roleName;
        this.roleValue = roleValue;
    }
}
