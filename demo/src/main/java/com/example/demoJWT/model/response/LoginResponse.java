package com.example.demoJWT.model.response;


import com.example.demoJWT.model.jpa.UserEntity;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class LoginResponse {

    @Expose
    private String token;
    @Expose
    private UserEntity userEntity;

}
