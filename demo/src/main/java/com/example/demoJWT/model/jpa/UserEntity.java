package com.example.demoJWT.model.jpa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "demojwt")
@Data
@NoArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private int userId;

    @Basic
    @Column(name = "user_code")
    @Expose
    private String userCode;

    @Column(name = "full_name")
    @Expose
    private String fname;

    @Basic
    @Column(name = "accountId")
    @Expose
    private Integer accountId;


    @Column(name = "avartar_url")
    @Expose
    private String avatarUrl;

    @ManyToOne(fetch = FetchType.EAGER) // co nhieu user co cung 1 role
    @JoinColumn(name = "roleId", referencedColumnName = "roleId", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @SerializedName("role")
    @Expose
    private RoleEntity roleByRoleId;

   public UserEntity(String userCode,Integer accountId, RoleEntity roleEntity){
       this.userCode = userCode;
       this.accountId = accountId;
       this.roleByRoleId = roleEntity;
   }
}
