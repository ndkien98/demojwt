package com.example.demoJWT.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountReponse {

    private String username;
    private String password;
    private int roleId;
}
