package com.example.demoJWT.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

    private int userId;

    private String fname;

    private Integer accountId;

    private String userCode;

    private MultipartFile avatarUrl;

    private int roleId;



}
