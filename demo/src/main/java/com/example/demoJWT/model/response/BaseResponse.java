package com.example.demoJWT.model.response;

import com.example.demoJWT.config.StatusCode;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class BaseResponse {

    @Expose
    private int errorCode;
    @Expose
    private Object message;
    @Expose
    private Object data;

    public BaseResponse() {
        this.errorCode = StatusCode.ERROR;
        this.message = "Error";
    }

}
