package com.example.demoJWT.service;

import com.example.demoJWT.dao.repository.RoleRepository;
import com.example.demoJWT.dao.repository.UserRepository;
import com.example.demoJWT.model.jpa.RoleEntity;
import com.example.demoJWT.model.jpa.UserEntity;
import com.example.demoJWT.model.request.UserRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ImageService imageService;

    public List<UserEntity> getAllUser() {
        return userRepository.findAll();
    }

    public UserEntity getUserById(int id) {
        return userRepository.findById(id).get();
    }

    public int createUser(UserRequest userRequest) throws IOException {
        try {
            UserEntity userEntity = modelMapper.map(userRequest, UserEntity.class);
            RoleEntity roleEntity = roleRepository.findById(userRequest.getRoleId()).get();
            userEntity.setRoleByRoleId(roleEntity);
            if (userRequest.getAvatarUrl() == null){
                System.out.println("null");
            }else {
                userEntity.setAvatarUrl(imageService.uploadImg(userRequest.getAvatarUrl()).getImageLink());
            }
            userRepository.save(userEntity);
            return userRepository.findByUserCode(userEntity.getUserCode()).getUserId();
        } catch (Exception e) {
            return -1;
        }
    }

    public UserEntity getUserByUserCode(String userCode) {
        UserEntity userEntity = userRepository.findByUserCode(userCode);
        if (userEntity == null) {
            return null;
        }
        return userEntity;
    }

}
