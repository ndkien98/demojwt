package com.example.demoJWT.service;

import com.example.demoJWT.config.Constant;
import com.example.demoJWT.model.response.ImageResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageService {

    public ImageResponse uploadImg(MultipartFile file) throws IOException {

        String partDirByDate = new File("").getAbsolutePath() + Constant.PATH + "/" + java.time.LocalDate.now();
        File fileCreateDir = new File(partDirByDate);
        if (!fileCreateDir.exists()) {
            fileCreateDir.mkdirs();
        }
        String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();
        String imagepath = partDirByDate + "/" + fileName;
        Path path = Paths.get(imagepath);
        byte[] bytes = file.getBytes();
        Files.write(path, bytes);

        ImageResponse imageResponse = ImageResponse.builder().imageLink(imagepath).imageName(file.getOriginalFilename()).imageSize(file.getSize()).build();
        return imageResponse;
    }
}
