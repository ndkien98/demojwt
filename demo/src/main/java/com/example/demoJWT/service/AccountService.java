package com.example.demoJWT.service;

import com.example.demoJWT.dao.repository.AccountReposiroty;
import com.example.demoJWT.model.jpa.AccountEntity;
import com.example.demoJWT.util.EncryptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

@Service
public class AccountService {

    @Autowired
    private AccountReposiroty accountReposiroty;


    public AccountEntity getAccountByUserId(int userId) {
        return accountReposiroty.getAccountByUserId(userId);
    }

    public AccountEntity getAccountByUsername(String username) {
        return accountReposiroty.getAccountByUserName(username);
    }


    public int checkLogin(String userName, String pass) {
        int userId = -1;
        try {
            AccountEntity accountEntity = accountReposiroty.getUserLogin(userName, EncryptUtils.crc32PasswordEncoder(pass));
            if (accountEntity != null) {
                userId = accountEntity.getUserId();
                if (userId == -1) {
                    System.out.println("login fail to check username");
                }
            }

        } catch (Exception e) {
            System.out.println("error when checklogin with username [{}] and pass [{}]: {}");
            e.printStackTrace();
        }
        return userId;
    }


}
