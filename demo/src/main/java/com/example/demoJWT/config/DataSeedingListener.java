package com.example.demoJWT.config;

import com.example.demoJWT.dao.repository.AccountReposiroty;
import com.example.demoJWT.dao.repository.RoleRepository;
import com.example.demoJWT.dao.repository.UserRepository;
import com.example.demoJWT.jwt.CustomPasswordEncoder;
import com.example.demoJWT.model.jpa.AccountEntity;
import com.example.demoJWT.model.jpa.RoleEntity;
import com.example.demoJWT.model.jpa.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.management.relation.Role;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private AccountReposiroty accountReposiroty;

    @Autowired
    public DataSeedingListener(UserRepository userRepository, RoleRepository roleRepository, AccountReposiroty accountReposiroty) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.accountReposiroty = accountReposiroty;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        CustomPasswordEncoder customPasswordEncoder = new CustomPasswordEncoder();

        if (roleRepository.findByRoleName("admin") == null) {
            roleRepository.save(new RoleEntity("admin", 1));
        }

        if (userRepository.findByUserCode("admin") == null) {
            userRepository.save(new UserEntity("admin", 1, roleRepository.findByRoleName("admin")));
        }

        if (accountReposiroty.getAccountByUserName("admin") == null) {
            accountReposiroty.save(new AccountEntity("admin", customPasswordEncoder.encode("admin"), roleRepository.findByRoleName("admin"), userRepository.findByUserCode("admin")));
        }


    }
}
