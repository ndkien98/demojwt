package com.example.demoJWT.service;

import com.example.demoJWT.dao.repository.AccountReposiroty;
import com.example.demoJWT.jwt.CustomPasswordEncoder;
import com.example.demoJWT.model.jpa.AccountEntity;
import com.example.demoJWT.util.EncryptUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Autowired
    AccountService accountService;

    CustomPasswordEncoder passwordEncoder;

    @Autowired
    AccountReposiroty accountReposiroty;


    @Autowired

    @Before
    public void setUp() throws Exception {
        this.passwordEncoder = new CustomPasswordEncoder();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getAccountByUsername(){
        AccountEntity accountEntity = accountService.getAccountByUsername("admin");
        System.out.println(accountEntity.getUserName());
    }

    @Test
    public void getAccountByUserId(){
        System.out.println(accountService.getAccountByUserId(1).toString());
    }

    @Test
    public void getPassword(){
        System.out.println(EncryptUtils.crc32PasswordEncoder("admin"));
    }

    @Test
    public void checkLogin(){

    }

    @Test
    public void checkPass(){
        AccountEntity accountEntity = accountService.getAccountByUsername("admin");
        Boolean check = this.passwordEncoder.matches("admin",accountEntity.getPasswordHash());
        System.out.println();
        System.out.println(check);
    }

    @Test
    public void getUserByUserNameAndPass(){
        AccountEntity accountEntity = accountReposiroty.getUserLogin("admin",EncryptUtils.crc32PasswordEncoder("admin"));
        if (accountEntity == null) {
            System.out.println(" null");
        }else System.out.println(accountEntity.toString());

    }

    @Test
    public void testURL(){
        String filePart = new File("").getAbsolutePath();
        filePart = filePart + "\\document\\img";
        System.out.println(filePart);
    }

}
