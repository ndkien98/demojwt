package com.example.demoJWT.service;

import com.example.demoJWT.dao.repository.RoleRepository;
import com.example.demoJWT.dao.repository.UserRepository;
import com.example.demoJWT.model.jpa.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;
    
    @Autowired
    RoleRepository roleRepository;

    @Test
    public void getUserById(){
        System.out.println(userService.getUserById(1));
    }

    @Test
    public void getUserByUserCode(){
        System.out.println(userService.getUserByUserCode("user01"));
    }
    
    @Test
    public void getRoleByRoleName(){
        System.out.printf(roleRepository.findByRoleName("admin").toString());
    }

}
