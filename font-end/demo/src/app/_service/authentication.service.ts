import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Base} from '../_model/Base';
import {map} from 'rxjs/operators';

// @ts-ignore
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    private http: HttpClient
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get getCurrentUser(): any {
    return this.currentUserSubject.value;
  }

  login(data: any) {
    return this.http.post<Base>(Base.URL + 'account/login', JSON.stringify(data), this.httpOptions)
      .pipe(map(reponse => {
        localStorage.setItem('currentUser', JSON.stringify(reponse.data));
        this.currentUserSubject.next(reponse.data);
        return reponse.data.userEntity;
      }))
      ;
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

}

