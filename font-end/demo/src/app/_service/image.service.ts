import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Base} from '../_model/Base';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ImageService {
  constructor(
    private http: HttpClient
  ) {
  }

  upload(file: any) {
    console.log(file);
    const formData = new FormData();
    formData.append('file', file);
    formData.forEach((value, key) => {
      console.log(key + ' ' + value);
    });
    // @ts-ignore
    // @ts-ignore
    return this.http.post<any>(Base.URL + 'image/update', formData).pipe(
      map(data => {
        console.log(data);
      })
    );
  }
}
