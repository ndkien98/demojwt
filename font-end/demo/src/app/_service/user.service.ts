import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Base} from '../_model/Base';
import {catchError, retry} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private http: HttpClient
  ){}

  createUser(data: any){
    return this.http.post<any>(Base.URL + 'user/create', JSON.stringify(data))
      .pipe(
        retry(1),
      );
  }
}
