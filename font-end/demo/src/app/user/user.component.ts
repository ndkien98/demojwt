import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../_service/authentication.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../_service/user.service';
import {ImageService} from "../_service/image.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userForm: FormGroup;
  currentFile: File;
  selectFiles: FileList;

  constructor(
    private builder: FormBuilder,
    private router: Router,
    private authentication: AuthenticationService,
    private userService: UserService,
    private imgService: ImageService
  ) {
  }

  ngOnInit(): void {
    this.createForm();
  }

  click() {
    this.router.navigate(['/login']);
    this.authentication.logout();
  }

  selectFile(event) {
    this.selectFiles = event.target.files[0];
  }

  createForm() {
    this.userForm = this.builder.group({
      fname: ['', [Validators.required]],
      userCode: ['', [Validators.required]],
      roleId: ['', [Validators.required]]
    });
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    // const user = new UserRequest(this.userForm.controls.fname.value,
    //   this.userForm.controls.userCode.value,
    //   this.currentFile,
    //   this.userForm.controls.roleId.value);
    // console.log(JSON.stringify(user));
    // this.userService.createUser(user).subscribe((data: any) => {
    //   console.log(data);
    // });
    // this.currentFile = this.selectFiles;
    this.imgService.upload(this.selectFiles).subscribe((data => {
      console.log(data);
    }));
  }
}
