import {LoginResponse} from './login_response';

export class Base {
  public static URL = 'http://localhost:8080/';
  errorCode: any;
  message: any;
  data: LoginResponse;
}
