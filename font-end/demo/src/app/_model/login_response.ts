import {User} from './user';

export class LoginResponse {
  token: any;
  userEntity: User;
}
