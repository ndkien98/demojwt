import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthenticationService} from '../_service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  status: boolean;

  constructor(
    private builder: FormBuilder,
    public http: HttpClient,
    private router: Router,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.status = false;
    this.createForm();
  }

  private createForm() {
    this.loginForm = this.builder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  public submit() {
    if (this.loginForm.status == 'INVALID') {
      this.status = true;
    } else {
      this.authenticationService.login(this.loginForm.value).subscribe((data: any) => {
          console.log(this.authenticationService.getCurrentUser);
          this.router.navigateByUrl('/user');
        },
        error1 => {
          alert('Username hoặc password không đúng');
        }
      );
    }
  }
}
