import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from 'rxjs';
import {AuthenticationService} from '../_service/authentication.service';


@Injectable({
  providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authentication: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userCurrent = this.authentication.getCurrentUser;
    if (userCurrent && userCurrent.token) {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json,image/jpeg',
          Authorization: `Bearer ${userCurrent.token}`,
        }
      });
    }
    return next.handle(req);
  }
}
