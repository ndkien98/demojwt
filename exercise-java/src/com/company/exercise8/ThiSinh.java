package com.company.exercise8;

import java.util.Scanner;

public class ThiSinh {

    private String sobaodanh;
    private String hoten;
    private String diachi;
    private String uutien;
    private String khoithi;


    public String getSobaodanh() {
        return sobaodanh;
    }

    public void setSobaodanh(String sobaodanh) {
        this.sobaodanh = sobaodanh;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getUutien() {
        return uutien;
    }

    public void setUutien(String uutien) {
        this.uutien = uutien;
    }

    public void nhapThongTin() {
        System.out.println("Nhập số báo danh");
        this.sobaodanh = new Scanner(System.in).nextLine();
        System.out.println("Nhập tên thí sinh");
        this.hoten = new Scanner(System.in).nextLine();
        System.out.println("Nhập địa chỉ");
        this.diachi = new Scanner(System.in).nextLine();
        System.out.println("Nhập độ ưu tiên");
        this.uutien = new Scanner(System.in).nextLine();
        System.out.println("Nhập vào khối thi : \n 1 : A \n 2 : B \n 3 : C \n Nhập 1 , 2 hoặc 3");
        int check = 0;
        check = new Scanner(System.in).nextInt();
        if (check == 1) this.khoithi = "Toán , lý , hóa";
        else if (check == 2) this.khoithi = "Toán ,hóa, sinh";
        else if (check == 3) this.khoithi = "Văn , sử , địa";
    }


    public void xuatThongTin() {
        System.out.println(this.toString());
    }

    public String getKhoithi() {
        return khoithi;
    }

    public void setKhoithi(String khoithi) {
        this.khoithi = khoithi;
    }

    @Override
    public String toString() {
        return "ThiSinh{" +
                "sobaodanh='" + sobaodanh + '\'' +
                ", hoten='" + hoten + '\'' +
                ", diachi='" + diachi + '\'' +
                ", uutien='" + uutien + '\'' +
                ", khoithi='" + khoithi + '\'' +
                '}';
    }
}
