package com.company.exercise8;

public class TuyenSinh {

    private ThiSinh[] thiSinhs;
    private int soLuong;

    public TuyenSinh(int soLuong) {
        this.soLuong = soLuong;
        thiSinhs = new ThiSinh[this.soLuong];
    }

    public void nhapDanhSachThiSinh() {
        System.out.println("Nhập danh sách thí sinh");
        for (int i = 0; i < soLuong; i++) {
            ThiSinh thiSinh = new ThiSinh();
            thiSinh.nhapThongTin();
            thiSinhs[i] = thiSinh;
        }
    }

    public void xuatDanhSachThiSinh() {
        System.out.println("\nDanh sách thí sinh");
        for (int i = 0; i < soLuong; i++) {
            thiSinhs[i].xuatThongTin();
        }
    }

    public void timKiemTheoMaThiSinh(String ma) {
        for (int i = 0; i < soLuong; i++) {
            if (thiSinhs[i].getSobaodanh().equals(ma)) {
                System.out.println("\nThí sinh tìm thấy là ");
                thiSinhs[i].xuatThongTin();
                return;
            }
        }
        System.out.println("\nKhông tìm thấy thí sinh");
    }

}
