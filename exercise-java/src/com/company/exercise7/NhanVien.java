package com.company.exercise7;

import java.util.Scanner;

public class NhanVien extends CanBo {

    private String congviec;

    public NhanVien() {
        super();
    }

    public String getCongviec() {
        return congviec;
    }

    public void setCongviec(String congviec) {
        this.congviec = congviec;
    }


    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        System.out.println("Nhập thông tin công việc");
        this.congviec = new Scanner(System.in).nextLine();
    }

    @Override
    public void xuatThongTin() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return super.toString() + "NhanVien{" +
                "congviec='" + congviec + '\'' +
                '}';
    }
}
