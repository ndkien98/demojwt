package com.company.exercise7;

import java.util.Scanner;

public class CanBo {

    private String hoten;
    private int namsinh;
    private String gioitinh;
    private String diachi;

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public int getNamsinh() {
        return namsinh;
    }

    public void setNamsinh(int namsinh) {
        this.namsinh = namsinh;
    }

    public String getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(String gioitinh) {
        this.gioitinh = gioitinh;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public void nhapThongTin() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên : ");
        this.hoten = scanner.nextLine();
        System.out.println("Nhập năm sinh  ");
        this.namsinh = scanner.nextInt();
        System.out.println("Nhập giới tính");
        this.gioitinh = new Scanner(System.in).nextLine();
        System.out.println("Nhập địa chỉ");
        this.diachi = new Scanner(System.in).nextLine();
    }

    public void xuatThongTin(){
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "CanBo{" +
                "hoten='" + hoten + '\'' +
                ", namsinh=" + namsinh +
                ", gioitinh='" + gioitinh + '\'' +
                ", diachi='" + diachi + '\'' +
                '}';
    }
}
