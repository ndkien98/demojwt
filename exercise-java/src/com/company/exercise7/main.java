package com.company.exercise7;

import java.util.Scanner;

public class main {

    /**
     * Một đơn vị sản xuất gồm có các cán bộ là công nhân, kỹ sư, nhân viên.
     * <p>
     * + Mỗi cán bộ cần quản lý lý các thuộc tính: Họ tên, năm sinh, giới tính, địa chỉ
     * <p>
     * + Các công nhân cần quản lý: Bậc (công nhân bậc 3/7, bậc 4/7 ...)
     * + Các kỹ sư cần quản lý: Ngành đào tạo
     * + Các nhân viên phục vụ cần quản lý thông tin: công việc
     * <p>
     * 1. Xây dựng các lớp NhanVien, CongNhan, KySu kế thừa từ lớp CanBo
     * 2. Xây dựng các hàm để truy nhập, hiển thị thông tin và kiểm tra về các thuộc tính của các lớp.
     * <p>
     * 3. Xây dựng lớp QLCB cài đặt các phương thức thực hiện các chức năng sau:
     * - Nhập thông tin mới cho cán bộ
     * - Tìm kiếm theo họ tên
     * - Hiển thị thông tin về danh sách các cán bộ
     * - Thoát khỏi chương trình
     */

    public static void main(String[] args) {


        QuanLyCanBo quanLyCanBo = new QuanLyCanBo(3);
        quanLyCanBo.nhapDanhSachCanBo();
        quanLyCanBo.xuatDanhSachCanBo();
        System.out.println("\nNhập tên cán bộ muốn tìm");
        String name = new Scanner(System.in).nextLine();
        CanBo canBo = quanLyCanBo.timCanBoTheoHoTen(name);
        if (canBo != null) {
            System.out.println("\nThông tin cán bộ là : ");
            canBo.xuatThongTin();
        } else {
            System.out.println("\nKhông tìm thấy cán bộ");
        }

        System.out.println("Bạn muốn tìm tiếp hay hủy");
        System.out.println("\n1 : Tìm lại");
        System.out.println("\n2 : Hủy");
        int check = new Scanner(System.in).nextInt();
        if (check == 1) {
            System.out.println("\nNhập tên cán bộ muốn tìm");
            String name1 = new Scanner(System.in).nextLine();
            CanBo canBo2 = quanLyCanBo.timCanBoTheoHoTen(name1);
            if (canBo2 != null) {
                System.out.println("\nThông tin cán bộ là : ");
                canBo2.xuatThongTin();
            } else {
                System.out.println("\nKhông tìm thấy cán bộ");
            }
        } else {
            return;
        }


    }

}
