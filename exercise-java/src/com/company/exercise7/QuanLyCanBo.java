package com.company.exercise7;

import java.util.Scanner;

public class QuanLyCanBo {

    private CanBo[] canBos;
    private int soluong;

    public QuanLyCanBo(int soluong) {
        this.soluong = soluong;
        this.canBos = new CanBo[this.soluong];
    }

    public void nhapDanhSachCanBo() {
        int check = 0;
        CanBo canBo = null;
        for (int i = 0; i < this.soluong; i++) {
            System.out.println("Nhập loại cán bộ mốn nhập : ");
            System.out.println("\n 1 : Công nhân");
            System.out.println("\n 2 : Kỹ sư");
            System.out.println("\n 3 : Nhân viên");
            do {
                if (check < 0 || check > 3) {
                    System.out.println("Bạn nhập không đúng loại nhân viên, đề nghị nhập lại\n");
                    check = new Scanner(System.in).nextInt();
                } else check = new Scanner(System.in).nextInt();
            } while (check < 0 || check > 3);

            if (check == 1) {
                canBo = new CongNhan();
            } else if (check == 2) {
                canBo = new KySu();
            } else if (check == 3) {
                canBo = new NhanVien();
            }
            canBo.nhapThongTin();
            canBos[i] = canBo;
            canBo = null;
        }
    }

    public void xuatDanhSachCanBo() {
        for (int i = 0; i < soluong; i++) {
            canBos[i].xuatThongTin();
        }
    }

    public CanBo timCanBoTheoHoTen(String hoten) {

        for (int i = 0; i < soluong; i++) {
            if (canBos[i].getHoten().toUpperCase().equals(hoten.toUpperCase())) {
                return canBos[i];
            }
        }
        return null;
    }

}
