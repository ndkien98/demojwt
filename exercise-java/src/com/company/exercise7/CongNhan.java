package com.company.exercise7;

import java.util.Scanner;

public class CongNhan extends CanBo {

    private String bac;

    public CongNhan() {
        super();
    }

    public String getBac() {
        return bac;
    }

    public void setBac(String bac) {
        this.bac = bac;
    }

    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        System.out.println("Nhập bậc của công nhân : ");
        this.bac = new Scanner(System.in).nextLine();
    }

    @Override
    public void xuatThongTin() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return super.toString() + "CongNhan{" +
                "bac='" + bac + '\'' +
                '}';
    }
}
