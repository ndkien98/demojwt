package com.company.exercise7;

import java.util.Scanner;

public class KySu extends CanBo {

    private String nganhdaotao;

    public KySu() {
        super();
    }

    public String getNganhdaotao() {
        return nganhdaotao;
    }

    public void setNganhdaotao(String nganhdaotao) {
        this.nganhdaotao = nganhdaotao;
    }

    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        System.out.println("Nhập ngành đào tạo");
        this.nganhdaotao = new Scanner(System.in).nextLine();
    }

    @Override
    public void xuatThongTin() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return super.toString() + "KySu{" +
                "nganhdaotao='" + nganhdaotao + '\'' +
                '}';
    }
}
