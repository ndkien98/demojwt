package com.company.exercise5;

import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class ChuyenXe {

    private static int auto = 0;
    private int maChuyen;
    private String hoTenTaiXe;
    private int soXe;
    private int soTuyen;
    private double doanhThu;
    private double doanhThuMotTuyen = 20000; // tự quy định doanh thu 1 tuyến để không cần nhập vào trường doanh thu , doanh thu = doanhThuMotTuyen * soTuyen

    public ChuyenXe(){
        auto ++;
       this.maChuyen = auto;
    }

    public int getMaChuyen() {
        return maChuyen;
    }

    public void setMaChuyen(int maChuyen) {
        this.maChuyen = maChuyen;
    }

    public String getHoTenTaiXe() {
        return hoTenTaiXe;
    }

    public void setHoTenTaiXe(String hoTenTaiXe) {
        this.hoTenTaiXe = hoTenTaiXe;
    }

    public int getSoXe() {
        return soXe;
    }

    public void setSoXe(int soXe) {
        this.soXe = soXe;
    }

    public int getSoTuyen() {
        return soTuyen;
    }

    public void setSoTuyen(int soTuyen) {
        this.soTuyen = soTuyen;
    }

    public double getDoanhThu() {
        return doanhThu;
    }

    public void setDoanhThu(double doanhThu) {
        this.doanhThu = doanhThu;
    }

    public void nhapThongTin(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên tài xế: ");
        this.hoTenTaiXe = scanner.nextLine();
        System.out.println("Nhập số xe: ");
        this.soXe = scanner.nextInt();
        System.out.println("Nhập số tuyến: ");
        this.soTuyen = scanner.nextInt();
        this.doanhThu = this.soTuyen * this.doanhThuMotTuyen;
    }

    public void xuatThongTin() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "ChuyenXe{" +
                "maChuyen=" + maChuyen +
                ", hoTenTaiXe='" + hoTenTaiXe + '\'' +
                ", soXe=" + soXe +
                ", soTuyen=" + soTuyen +
                ", doanhThu=" + doanhThu +
                '}';
    }
}
