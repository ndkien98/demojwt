package com.company.exercise5;

import java.util.Scanner;

public class NoiThanh extends ChuyenXe {

    private int soKm;

    public int getSoKm() {
        return soKm;
    }

    public void setSoKm(int soKm) {
        this.soKm = soKm;
    }

    public NoiThanh(){
        super();
    }

    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        System.out.println("Nhập số km đi được : ");
        this.soKm = new Scanner(System.in).nextInt();
    }

    @Override
    public void xuatThongTin() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return super.toString() +"NoiThanh{" +
                "soKm=" + soKm +
                '}';
    }
}
