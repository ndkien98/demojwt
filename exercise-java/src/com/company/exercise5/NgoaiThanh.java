package com.company.exercise5;

import java.util.Scanner;

public class NgoaiThanh extends ChuyenXe {

    private int soNgayDi;

    public int getSoNgayDi() {
        return soNgayDi;
    }

    public void setSoNgayDi(int soNgayDi) {
        this.soNgayDi = soNgayDi;
    }

    public NgoaiThanh() {
        super();
    }

    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        System.out.println("Nhập số ngày đi: ");
        this.soNgayDi = new Scanner(System.in).nextInt();
    }

    @Override
    public void xuatThongTin() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return super.toString() + "NgoaiThanh{" +
                "soNgayDi=" + soNgayDi +
                '}';
    }
}
