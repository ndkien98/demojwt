package com.company.exercise5;

public class main {

    /**5
     *  Công ty du lịch A quản lý thông tin là các chuyến xe. Thông tin của 2 loại chuyến xe:
     * - Chuyến xe nội thành: Mã số chuyến, Họ tên tài xế, số xe, số tuyến, số km đi được, doanh thu
     * - Chuyến xe ngoại thành: Mã số chuyến, Họ tên tài xế, số xe, nơi đến, số ngày đi được, doanh
     * thu
     * - Xây dựng các lớp với chức năng kế thừa
     * - Viết chương trình quản lý các chuyến xe theo dạng cây kế thừa với các phương thức sau:
     * + Nhập, xuất danh sách các chuyến xe
     * + Tính tổng doanh thu cho từng loại xe
     *
     * - chuyến xe : mã chuyến xe , họ tên tài sế , số xe , số tuyến , doanh thu
     *
     * + xe nội thành : số km đi được
     * + xe ngoại thành : số ngày đi được
     *
     */


    public static void main(String[] args) {

//        QuanLyXe quanLyXe = new QuanLyXe(3);
//        System.out.println("Thong bao");
//        ChuyenXe chuyenXe = new ChuyenXe();
//        chuyenXe.nhapThongTin();
//        chuyenXe.xuatThongTin();
//        chuyenXe = new NoiThanh();
//        chuyenXe.nhapThongTin();
//        chuyenXe.xuatThongTin();
//        chuyenXe = new NgoaiThanh();
//        chuyenXe.nhapThongTin();
//        chuyenXe.xuatThongTin();

        // contructor truyen vao so luong xe
    QuanLyXe quanLyXe = new QuanLyXe(3);
    quanLyXe.nhapDanhSachXe();
    quanLyXe.xuatDanhSachXe();

    }

}
