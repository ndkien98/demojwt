package com.company.exercise5;

import java.util.Scanner;

public class QuanLyXe {

    private ChuyenXe[] danhSachXe;
    private int soXe;

    public QuanLyXe(int soxe) {
        this.soXe = soxe;
        this.danhSachXe = new ChuyenXe[soxe];
    }

    public QuanLyXe() {
        this.danhSachXe = new ChuyenXe[100];
    }

    public QuanLyXe(ChuyenXe[] danhSachXe) {
        this.danhSachXe = danhSachXe;
    }

    public void nhapDanhSachXe() {
        ChuyenXe chuyenXe = null;
        int loaiXe = 0;
        for (int i = 0; i < soXe; i++) {
            System.out.println("Nhập 1 hoặc 2 để chọn loại xe \n");
            System.out.println("1 : Xe trong nội thành\n");
            System.out.println("2 : Xe ngoại thành\n");
            loaiXe = new Scanner(System.in).nextInt();
            if (loaiXe == 1) {
                System.out.println("Nhập thông tin xe nội thành\n");
                chuyenXe = new NoiThanh();
                chuyenXe.nhapThongTin();
            } else if (loaiXe == 2) {
                System.out.println("Nhập thông tin xe ngoại thành\n");
                chuyenXe = new NgoaiThanh();
                chuyenXe.nhapThongTin();
            }
            danhSachXe[i] = chuyenXe;
            chuyenXe = null;
        }
    }

    public void xuatDanhSachXe() {
        double doanhThuXeNoiThanh = 0, doanhThuXeNgoaiThanh = 0, doanhThuXe = 0;
        for (int i = 0; i < soXe; i++) {
            if (danhSachXe[i] instanceof NoiThanh) {
                doanhThuXeNoiThanh += danhSachXe[i].getDoanhThu();
            } else if (danhSachXe[i] instanceof NgoaiThanh) {
                doanhThuXeNgoaiThanh += danhSachXe[i].getDoanhThu();
            }
            danhSachXe[i].xuatThongTin();
        }
        System.out.println("\nDanh thu xe nội thành : " +doanhThuXeNoiThanh);
        System.out.println("\nDoanh thu xe ngoại thành : " + doanhThuXeNgoaiThanh);
    }

}
