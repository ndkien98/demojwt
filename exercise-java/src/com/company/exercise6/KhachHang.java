package com.company.exercise6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class KhachHang {

    private static int auto;

    private int id;
    private String hoTen;
    private Date ngayRaHoaDon;
    private int soLuong;
    private int donGia;
    private double thanhTien;

    public KhachHang() {
        auto ++;
        this.id = auto;
    }

    public double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public Date getNgayRaHoaDon() {
        return ngayRaHoaDon;
    }

    public void setNgayRaHoaDon(Date ngayRaHoaDon) {
        this.ngayRaHoaDon = ngayRaHoaDon;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public void nhapThongTin(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên khách hàng : ");
        this.hoTen = scanner.nextLine();
        System.out.println("Nhập ngày ra hóa đơn");
        int ngay = scanner.nextInt();
        System.out.println("Nhập tháng ra hóa đơn");
        int thang = scanner.nextInt();
        System.out.println("Nhập năm ra hóa đơn ");
        int nam = scanner.nextInt();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
        String dataConvert = Integer.toString(ngay) + "/" + Integer.toString(thang) + "/" + Integer.toString(nam);
        try {
            ngayRaHoaDon = simpleDateFormat.parse(dataConvert);
        }catch (ParseException e){
            System.out.println("Nhập vào ngày tháng năm sai định dạng");
        }
        System.out.println("Nhập vào số lượng");
        this.soLuong = scanner.nextInt();
        System.out.println("Nhập vào đơn giá ");
        this.donGia = scanner.nextInt();
    }


    public void xuatThongTin(){
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
        return "KhachHang{" +
                "id=" + id +
                ", hoTen='" + hoTen + '\'' +
                ", ngayRaHoaDon=" + simpleDateFormat.format(ngayRaHoaDon) +
                ", soLuong=" + soLuong +
                ", donGia=" + donGia +
                '}';
    }

    public double tinhThanhTien(){
        return thanhTien = 0;
    }
}
