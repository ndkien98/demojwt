package com.company.exercise6;

public class main {


    /**
     * Xây dựng chương trình quản lý danh sách hoá đơn tiền điện của khách hàng. Thông
     * tin bao gồm các loại khách hàng:
     * - Khách hàng Việt Nam: mã khách hàng, họ tên, ngày ra hoá đơn (ngày, tháng, năm), đối tượng
     * khách hàng (sinh hoạt, kinh doanh, sản xuất): số lượng (số KW tiêu thụ), đơn giá, định mức. Thành
     * tiền được tính như sau:
     *
     * + Nếu số lượng <= định mức thì: thành tiền = số lượng * đơn giá.
     * + Ngược lại thì: thành tiền = số lượng * đơn giá * định mức + số lượng KW vượt định mức *
     * Đơn giá * 2.5
     *
     * - Khách hàng nước ngoài: mã khách hàng, họ tên, ngày ra hoá đơn (ngày, tháng, năm), quốc
     * tịch, số lượng, đơn giá. Thành tiền được tính = số lượng * đơn giá.
     * Thực hiện các yêu cầu sau:
     *
     * - Xây dựng các lớp với chức năng thừa kế.
     * - Nhập xuất danh sách các hóa đơn khách hang
     * - Tính tổng số lượng cho từng loại khách hang
     * - Tính trung bình thành tiền của khách hàng người nước ngoài
     * - Xuất ra các hoá đơn trong tháng 09 năm 2013 (cùa cả 2 loại khách hàng).
     * @param args
     *
     * khách hàng : mã khách hàng, họ tên , ngày ra hóa đơn ( ngày , tháng , năm ), số lượng ,đơn giá
     * khách hàng việt nam : đối tượng khách hàng ( sinh hoạt , kinh doanh , sản xuất)  , định mức
     * khách hàng nước ngoài : quốc tịch
     *
     *
     */

    public static void main(String[] args) {
        // contructor truyen vao so luong khach hang
       QuanLy quanLy = new QuanLy(3);
       quanLy.nhapDanhSachKhachHang();
       quanLy.xuatDanhSachKhachHang();
       // tham so truyen cao chuoi thang , nam
       quanLy.xuatRaHoaDonTheoThangAndNam("09/2013");

    }
}
