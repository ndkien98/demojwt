package com.company.exercise6;

import java.util.Scanner;

public class KhachHangVietNam extends KhachHang {

    private int doiTuong;
    private int dinhMuc;

    public KhachHangVietNam() {
        super();
    }

    public int getDoiTuong() {
        return doiTuong;
    }

    public void setDoiTuong(int doiTuong) {
        this.doiTuong = doiTuong;
    }

    public int getDinhMuc() {
        return dinhMuc;
    }

    public void setDinhMuc(int dinhMuc) {
        this.dinhMuc = dinhMuc;
    }

    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập thông tin đối tượng khách hàng");
        System.out.println("\n1 : Đối tượng sinh hoạt");
        System.out.println("\n2 : Đối tượng kinh doanh");
        System.out.println("\n3 : Đối tượng sản xuất");
        do {
            if (this.doiTuong > 3 || this.doiTuong < 0) {
                System.out.println("\nĐề nghị nhập đúng loại đối tượng 1 hoặc 2, hoặc 3");
                this.doiTuong = scanner.nextInt();
            } else {
                this.doiTuong = scanner.nextInt();
            }
        } while (this.doiTuong > 3 || this.doiTuong < 0);
        System.out.println("Nhập định mức : ");
        this.dinhMuc = scanner.nextInt();
    }


    @Override
    public void xuatThongTin() {
        System.out.println(this.toString());
        System.out.println("\nThành tiền : " + this.getThanhTien());
    }

    @Override
    public String toString() {
        String doituong = null;
        if (this.doiTuong == Constant.KINH_DOANH) {
            doituong = "Kinh doanh";
        } else if (this.doiTuong == Constant.SAN_XUAT) {
            doituong = "Sarn xuất";
        } else if (this.doiTuong == Constant.SINH_HOAT) {
            doituong = "Sinh hoạt";
        }
        return super.toString() + "KhachHangVietNam{" +
                "doiTuong=" + doituong +
                ", dinhMuc=" + dinhMuc +
                '}';
    }

    @Override
    public double tinhThanhTien() {
        if (this.getSoLuong() <= this.getDinhMuc()) {
            this.setThanhTien(this.getSoLuong() * this.getDonGia());
        } else if (this.getSoLuong() > this.getDinhMuc()) {
            this.setThanhTien(
                    this.getSoLuong() * this.getDonGia() * this.getDinhMuc()
                            + ((this.getSoLuong() - this.getDinhMuc()) * this.getDonGia() * 2.5));
        }
        return this.getThanhTien();
    }
}
