package com.company.exercise6;

import java.text.SimpleDateFormat;
import java.util.Scanner;

public class QuanLy {

    private KhachHang[] khachHangs;
    private int soLuong;

    public QuanLy(int soLuong) {
        this.soLuong = soLuong;
        this.khachHangs = new KhachHang[this.soLuong];
    }

    public void nhapDanhSachKhachHang() {
        KhachHang khachHang = null;
        int check = 0;
        for (int i = 0; i < this.soLuong; i++) {
            System.out.println("Nhập vào loại khách hàng: \n");
            System.out.println("\n1 : khách hàng nước ngoài");
            System.out.println("\n2 : khách hàng việt nam");
            do {
                if (check < 0 || check > 2) {
                    System.out.println("\nBạn nhập loại khách hàng không chính xác đề nghị nhập lại");
                    check = new Scanner(System.in).nextInt();
                } else {
                    check = new Scanner(System.in).nextInt();
                }

            } while (check < 0 || check > 2);

            if (check == 1) {
                khachHang = new KhachHangNuocNgoai();
                khachHang.nhapThongTin();
            } else if (check == 2) {
                khachHang = new KhachHangVietNam();
                khachHang.nhapThongTin();
            }
            khachHangs[i] = khachHang;
        }
    }

    public void xuatDanhSachKhachHang() {
        int soLuongKhachVietNam = 0, soLuongkhachNuocNgoai = 0;
        for (int i = 0; i < soLuong; i++) {
            khachHangs[i].xuatThongTin();
            if (khachHangs[i] instanceof KhachHangVietNam) {
                soLuongKhachVietNam++;
            } else if (khachHangs[i] instanceof KhachHangNuocNgoai) {
                soLuongkhachNuocNgoai++;
            }
        }
        System.out.println("\n Tổng số lượng khách hàng việt nam : " + soLuongKhachVietNam);
        System.out.println("\nTổng số lượng khách hàng nước ngoài : " + soLuongkhachNuocNgoai);
        System.out.println("\nTổng tiền trung bình của khách nước ngoài là : " + tinhTrungBinhThanhTienKhacNuocNgoai());
    }

    private double tinhTrungBinhThanhTienKhacNuocNgoai() {
        double tongTien = 0, tongKhach = 0;
        for (int i = 0; i < soLuong; i++) {
            if (khachHangs[i] instanceof KhachHangNuocNgoai) {
                tongTien += khachHangs[i].getThanhTien();
                tongKhach++;
            }
        }
        return tongTien / tongKhach;
    }

    public void xuatRaHoaDonTheoThangAndNam(String thangNam) {
        System.out.println("\n-------------Hóa đơn theo " + thangNam);
        StringBuffer date = new StringBuffer("dd/mm/yyyy");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(date.toString());
        for (int i = 0; i < soLuong; i++) {
            date.replace(0, 10, simpleDateFormat.format(khachHangs[i].getNgayRaHoaDon()));
            if (date.substring(3).equals(thangNam)) {
                khachHangs[i].xuatThongTin();
            }
        }


    }


}
