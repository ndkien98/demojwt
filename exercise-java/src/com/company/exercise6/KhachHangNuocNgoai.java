package com.company.exercise6;

import java.util.Scanner;

public class KhachHangNuocNgoai extends KhachHang {

    private String quocTich;

    public KhachHangNuocNgoai() {
        super();
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }

    @Override
    public void nhapThongTin() {
        super.nhapThongTin();
        System.out.println("Nhập quốc tịch : ");
        this.quocTich = new Scanner(System.in).nextLine();
    }


    @Override
    public void xuatThongTin() {
        System.out.println(this.toString());
        System.out.println("\nThành tiền : " +this.tinhThanhTien());
    }

    @Override
    public String toString() {
        return super.toString()+ "KhachHangNuocNgoai{" +
                "quocTich='" + quocTich + '\'' +
                '}';
    }

    @Override
    public double tinhThanhTien() {
        this.setThanhTien(this.getSoLuong()* this.getDonGia());
        return this.getThanhTien();
    }
}
